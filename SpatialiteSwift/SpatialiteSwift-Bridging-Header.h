//
//  SpatialiteSwift-Bridging-Header.h
//  SpatialiteSwift
//
//  Created by Jaim Zuber on 12/2/16.
//  Copyright © 2016 Sharp Five Software. All rights reserved.
//

#ifndef SpatialiteSwift_Bridging_Header_h
#define SpatialiteSwift_Bridging_Header_h

#include <sqlite3.h>
#include <spatialite/gaiageo.h>
#include <spatialite.h>

void spatialite_init (int verbose);

#endif /* SpatialiteSwift_Bridging_Header_h */
